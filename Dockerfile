FROM igwn/lalsuite-dev:el7

LABEL name="LALSuite Development - ICC"
LABEL maintainer="Duncan Macleod <duncan.macleod@ligo.org>"
LABEL support="Best Effort"

# add Intel oneAPI
ADD oneAPI.repo /etc/yum/repos.d/
RUN yum install -y --disablerepo=\* --enablerepo=oneAPI \
        intel-oneapi-compiler-dpcpp-cpp-and-cpp-classic \
        intel-oneapi-mkl-devel && \
    yum clean all

# add oneAPI environment variables
ENV CPATH="/opt/intel/oneapi/compiler/latest/linux/include:/opt/intel/oneapi/dev-utilities/latest/include:/opt/intel/oneapi/mkl/latest/include"
ENV LD_LIBRARY_PATH="/opt/intel/oneapi/compiler/latest/linux/lib:/opt/intel/oneapi/compiler/latest/linux/lib/x64:/opt/intel/oneapi/compiler/latest/linux/lib/emu:/opt/intel/oneapi/compiler/latest/linux/compiler/lib/intel64_lin:/opt/intel/oneapi/compiler/latest/linux/compiler/lib:/opt/intel/oneapi/mkl/latest/lib/intel64:/opt/intel/oneapi/mkl/latest/lib"
ENV LIBRARY_PATH="/opt/intel/oneapi/compiler/latest/linux/compiler/lib/intel64_lin:/opt/intel/oneapi/compiler/latest/linux/lib:/opt/intel/oneapi/mkl/latest/lib/intel64:/opt/intel/oneapi/mkl/latest/lib"
ENV MANPATH="/opt/intel/oneapi/compiler/latest/documentation/en/man/common:/usr/share/man:/usr/local/share/man"
ENV ONEAPI_ROOT="/opt/intel/oneapi"
ENV PATH="/opt/intel/oneapi/compiler/latest/linux/bin/intel64:/opt/intel/oneapi/compiler/latest/linux/bin:/opt/intel/oneapi/compiler/latest/linux/ioc/bin:/opt/intel/oneapi/dev-utilities/latest/bin:${PATH}"

# set default compilers
ENV CC="icc"
ENV CXX="icpc"
ENV AR="xiar"
ENV LD="xild"
ENV LDSHARED="icc -shared"
